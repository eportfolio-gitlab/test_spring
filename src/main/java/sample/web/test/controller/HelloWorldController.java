package sample.web.test.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;



@Controller
public class HelloWorldController {

	@RequestMapping("/")
	public ModelAndView helloWorld() {

		ModelAndView mav = new ModelAndView("helloView");

		mav.addObject("message1", "Hello World.<strong>StringMVC 3.0!</strong>");

		mav.getModelMap().put("message2", "Some Test");

		mav.addObject("currentDate", new Date());

		return mav;
	}

}
