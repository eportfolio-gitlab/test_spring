<%@ page language="java" trimDirectiveWhitespaces="true" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<%@ include file="/include/taglibs.inc.jsp"%>

<html>
<head>
    <title>Spring 3.0 MVC : Hello World</title>
</head>
<body>
<p>Some body:</p>
<ul>
    <li>message1=${message1}</li>
    <li>message1(new)=<c:out value="${message1}" /></li>
    <li>message2=${message2}</li>
    <li>currentDate(formated)=<fmt:formatDate value="${currentDate}" pattern="yyyy MM dd"/></li>
</ul>
</body>
</html>